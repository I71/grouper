﻿#Requires -Modules gShell

# Author: Asher Dratel

# Adds input users to input groups. 
# Users and groups should be comma-separated lists, with/without spaces, and enclosed in "
# Users and Groups can both be entered as full e-mail addresses or just the name preceeding the @domain.com

function Set-GroupMembership {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory=$True,Position=1,HelpMessage="Users to add")]
        [string]$users,

        [Parameter(Mandatory=$True,Position=2,HelpMessage="Groups to add users to")]
        [string]$groups,

        [Parameter(Mandatory=$False)]
        [switch]$creategroup
    )

    $script:usrCount = 0
    $script:grpCountArr = @()
    $script:grpCreateCount = 0

    # Strip whitespace from the inputs, then take the strings and break them into elements
    $userList = ($users.replace(' ','')).Split(",")
    $groupList = ($groups.replace(' ','')).Split(",")

    # Function to create a group if it doesn't already exist and we've added the -creategroup flag
    # The created group will only allow members to view messages/posts, will allow any org members to ask to join, and will allow public posting
    # Once the group is made, re-attempt to add the user
    function New-Group([string]$grpNameCreate) {
        try {
            New-GAGroup -Email $grpNameCreate
            Write-Output "`t$grpNameCreate created"
            New-Member $usr $grpNameCreate
            $script:grpCreateCount++
        }
        catch [Google.GoogleApiException] {
            $errCode = $_.exception.error.code
            $errMsg = $_.exception.error.message
            Write-Output "`tErr $errCode, $errMsg"
        }
    }

    # Function to add members to the group
    function New-Member([string]$usrName,[string]$grpNameAdd) {
        try {
            Add-GAGroupMember -GroupName $grpNameAdd -UserName $usrName | Out-Null
            Write-Output "`tAdded to $grpNameAdd"
            if ($script:grpCountArr -inotcontains $grpNameAdd) {
                $script:grpCountArr += "$grpNameAdd"
            }
        }
        catch [Google.GoogleApiException] {
            $errCode = $_.exception.error.code
            $errMsg = $_.exception.error.message
            if ($errCode -eq 404 -and $creategroup) {
                New-Group $grpNameAdd
            }
            else {
                Write-Output "`tErr $errCode, $errMsg - $grpNameAdd"
            }
        }
    }

    # Nested foreach loops and try/catch blocks!
    # Run through each input username and check if they exist, then add them to the group if they do

    foreach ($usr in $userList) {
        #Check that the user exists using Get-GAUser
        try {
            Get-GAUser -UserKey $usr | Out-Null
            Write-Output "`nUser: $usr"
            $script:usrCount++
            # ...and add the user to the listed groups.
            foreach ($grp in $groupList) {
                New-Member $usr $grp
            }
        }
        catch [Google.GoogleApiException] {
            try {
                # If not a user account, see if it's a group
                Get-GAGroup -GroupName $usr | Out-Null
                Write-Output "`nUser: $usr"
                $script:usrCount++
                # ...and add the group to the listed groups.
                foreach ($grp in $groupList) {
                    New-Member $usr $grp
                }
            }
            catch {
            $errCode = $_.exception.error.code
            $errMsg = $_.exception.error.message
            Write-Output "`nErr: $errCode, $errMsg - $usr"
            }
        }
    }

    $grpCount = $script:grpCountArr.count
    Write-Output "`n$script:usrCount user(s) added to $grpCount group(s) ($script:grpCreateCount created.)"
}

Export-ModuleMember -Function Set-GroupMembership