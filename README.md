# README #

Here's how this all works.

### What's new? ###

* As of 2/16/2017, Grouper comes in plain ol' script form as well as module form. I recommend using the module!
* As of 8/25/2016, Grouper uses gShell instead of GAM. Error checking is much easier, and the entire syntax of the script is much more readable and pleasant.

### What is this repository for? ###

* Add user(s) to group(s) in Google Apps without having to go through 80 links and copy/paste addresses several times

### Requirements ###

* You'll need to download, install, and configure [gShell](https://github.com/squid808/gShell)

### Initial setup ###

* To set up the module, move the SetGroupmembership folder to your PowerShell module directory (can be found by entering $env:PSModulePath in the console), then use the following command to import the module:
```
#!powershell

Import-Module SetGroupMembership
```


### Grouper usage ###

* Grouper is very simple given that it is only meant to do one thing
* Commands are -users, -groups, and -creategroup
* Users and Groups can be single or multiple entries, multiple entries must be enclosed in double quotes
* Example: To add john.smith@company.com and jane.doe@company.com to the generic.names@company.com list, the command would be 
```
#!powershell

Set-GroupMembership -users "john.smith, jane.doe" -groups generic.names
```

* The -creategroup sets a flag in the script so that an input group that doesn't already exist will be created.

### GrouperFormatter ###

* When people send in tickets for adding people to groups, usually they don't format it in a list that can be dumped directly into Grouper
* GrouperFormatter strips out spaces and newlines, and replaces them with . and , respectively
* This assumes a certain e-mail formatting, generally first.last, so that if somebody sends you a request to add
    John Smith
    Jane Doe
  to an e-mail group, you can just dump that into the input section in GrouperFormatter and have it spit out john.smith,jane.doe and be ready to go

### Known unknowns/issues ###

* Groups aren't being counted properly in the module version. I assume a variable scope is wrong, I just haven't gotten a chance to troubleshoot it.
* I have no knowledge of using gShell across multiple Google Apps domains, so I can't say anything about how Grouper will work in such a situation.